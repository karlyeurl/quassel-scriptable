//! This is the fallback version number in case we can't autogenerate one
baseVersion = "0.10.0";

protocolVersion = 10;       //< Version of the client/core protocol
coreNeedsProtocol   = 10;   //< Minimum protocol version the core needs
clientNeedsProtocol = 10;   //< Minimum protocol version the client needs

distCommittish = 575f27ef7b7b335b56c5045d0b2c6514855ec82a
distCommitDate = 1395787390
