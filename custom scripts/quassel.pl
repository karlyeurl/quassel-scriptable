use strict;
use warnings;
 
use Cwd 'abs_path';
use MIME::Base64;
 
my $path = abs_path($0);
$path =~ s@[^/]+$@@;
my ($type, $buffer, $sender, $contents) = @ARGV;
my $resp = "";
 
$buffer = decode_base64($buffer);
$sender = decode_base64($sender);
$sender =~ s/\!.*//;
$contents = decode_base64($contents);


sub RandomLine{
   srand;
   open FILE, $_[0];
   my $it; 
   rand($.) < 1 && ($it = $_) while <FILE>;
   return "« $it »";
}


if ($type == 0x00001) { # PLAIN
        # when someone types !fortune, gets a random line from fortune.data
        if ($contents =~ /^!fortune/) {
                $resp = RandomLine($path."fortunes.data"); #gets random line in fortunes.data
        }

}
 
#if ($type == 0x04000) { # TOPIC
#}
 
if ($resp eq "") {
        print "__NOTHING__\n";
} else {
        print "OK " . encode_base64($resp);
}
