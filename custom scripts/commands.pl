use strict;
use warnings;
 
use Cwd 'abs_path';
use MIME::Base64;
 
my $path = abs_path($0);
$path =~ s@[^/]+$@@;
my ($contents) = @ARGV;
my $resp = "";
 
$contents = decode_base64($contents);

if ($contents eq "/mpv") {
 $resp = `/home/karl/.quassel/scripts/mpv.py`;#change this
}
 
if ($contents eq "/mpd") {
 $resp = `/home/karl/.quassel/scripts/mpd.py`;#change this
}
 
if ($resp eq "") {
        print "__NOTHING__\n";
} else {
        print "OK " . encode_base64($resp);
}
