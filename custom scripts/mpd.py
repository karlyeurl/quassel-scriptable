#!/usr/bin/python
# -*- coding: utf-8 -*-

import os

output = os.popen("mpc").readlines()
if len(output) > 1:
   status = output[0].strip("\n")
   progress = ' '.join(output[1].split()).strip("\n").split('/', 2)
   totaltime = progress[2].split(" ")[0]
   progress = progress[1].split(" ")[1]
   progress = int(progress.split(":")[0])*60 + int(progress.split(":")[1])
   totaltime = int(totaltime.split(":")[0])*60 + int(totaltime.split(":")[1])
   ratio = 20 * progress/totaltime

   advance = ""
   for x in range (0, 21):
      if x < ratio:
         advance += "─"
      elif (x-1 < ratio) and (x > ratio):
         advance += "╼"
      else:
         advance += " "
   print("/me is listening to %s ♫♪" % (status + " [" + advance + "]"))
